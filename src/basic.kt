/**
 * Created by kunihiro on 2017/05/19.
 */

fun main(args: Array<String>) {
    //文字列
    val name = "hanako"
    println("Hello," + name + "!")
    println("Hello,${name}!")
    println("hello,$name!")
    // """で文字列
    val text1 = """
    var name = "hanako"
    $name
    """
    println(text1)

    val text2 = """
    |var name = "hdanako"
    |$name
    """.trimMargin()
    println(text2)
    //配列
    var ints = arrayOfNulls<Int>(5)
    println(ints.size)
    println(ints[0])
    ints[0] = 123
    println(ints[0])
    //arrayOf こっちでいいや
    var strs = arrayOf("red","green","blue")
    println(strs[1])
    //リスト
    //Listは変えられない
    var list: List<Int> = listOf(1,2,3)
    println(list)
    println(list.size)
    println(list[0])

    //MutableList 変えられるlist
    var chars: MutableList<Char> = mutableListOf('c', 'b')
    println(chars)
    //要素を加える
    chars.add('k')
    println(chars)
    //要素を消す
    chars.removeAt(1)
    println(chars)

    //MAP
    val numberMap: MutableMap<String, Int> = mutableMapOf("one" to 1, "two" to 2)
    println(numberMap);
    println(numberMap.size)

    //レンジ
    // 5は1から10の中にあるか
    var aaa = 5 in 1..10
    println(aaa)

    var list2 = (1..5).toList()
    println(list2)


    println((5 downTo 1).toList())

    //増え具合ヘリ具合
    println((1..5 step 2).toList())

    //when つまりswitch
    //2とか3みたいに
    var x = 3
    println(when (x){
        1 -> "one"
        2,3 -> "two or three"
        else -> {
            "unknown"
        }
    })
    //さらに定数以外も使える
    println(when (x){
        1 -> "one"
        2 -> "two or three"
        in 2..4 -> "ほら定数も使えた"
        else -> {
            "unknown"
        }
    })

    //ループ
    //while
    var count = 3
    while (count-- > 0) {
        println(count)
    }
    //for
    for (x in arrayOf(1,2,3)){
        println(x)
    }
    val names = listOf("foo","bar","bas")
    for (name in names){
        println(name)
    }
    for (i in 1..10 step 2) {
        println(i)
    }
    for (item in MyIterable()){
        println(item)
    }

    //可変長引数
    //引数の数が可変
    fun sum(vararg ints: Int): Int{
        var sum = 0
        for (i in ints){
            sum += i
        }
        return sum
    }
    //呼び出す
    println(sum(1,1,1,1,1,1,1,1))
    //配列でもok *をつけてあげる
    println(sum(*intArrayOf(1,3,5)))

    //再帰呼び出し
    //まずは普通
    fun sum2 (numbers: List<Long>) : Long{
        var sum = 0L
        for (n in numbers){
            sum += n
        }
        return sum
    }

    println(sum2(arrayListOf(3,30,2)))

    //再帰にしてみる
    fun sum3(numbers: List<Long>) : Long {
        var sum = 0L
        if (numbers.isEmpty()) {
            sum += 0
        }
        else {
            sum += numbers.first() + sum3(numbers.drop(1))
        }
        return sum
    }

    println(sum3(arrayListOf(399,30,2)))

    //p82 関数オブジェクト
    fun square(i: Int): Int = i * i

    //::で関数オブジェクトを取得
    //別の変数に取得した関数を代入

    val functionObject = ::square
    println(functionObject(3))

    //関数の型 Intの引数を引き取ってIntを返す
    var functionObject2: (Int) -> Int = ::square
    println(functionObject2(10))

    //P84 最初にKが出現する位置を返す関数
    fun firstK(str: String): Int{
        tailrec fun go(str: String, index: Int): Int =
                when{
                    str.isEmpty() -> -1
                    str.first() == 'K' -> index
                    else -> go(str.drop(1), index + 1)
                }
        return go(str,0)
    }
    println(firstK("ABCWWK"))

    //最初が大文字の場合
    fun firstUpperCase(str: String): Int{
        tailrec fun go(str: String, index: Int): Int =
                when{
                    str.isEmpty() -> -1
                    str.first().isUpperCase() -> index
                    else -> go(str.drop(1), index + 1)
                }
        return go(str,0)
    }
    println(firstUpperCase("aaaaK"))

    //上二つのKがでるか大文字かだけなので二つ目の引数を汎用的にする
    fun first(str: String, predicate: (Char)->Boolean):Int {
        tailrec fun go(str: String, index: Int): Int {
            return when{
                str.isEmpty() -> -1
                predicate(str.first()) -> index
                else -> go(str.drop(1),index +1)
            }
        }
        return go(str,0)
    }
    //firstを読んだK
    fun firstK2(str:String) :Int{
        fun isK(c: Char): Boolean = c =='K'
        return first(str, ::isK)
    }
    println(firstK2("AAAAAAK"))
    //firstを呼んだupper
    fun firstUpperCase2(str: String): Int{
        fun isUpperCase(c:Char): Boolean = c.isUpperCase()
        return first(str, ::isUpperCase)
    }
    println(firstUpperCase2("ddfdfdfdfdfdAAAAAAK"))


    //P87 ラムダ式
    //関数オブジェクトを直接生成するコードをラムダ式という
    val square: (Int) -> Int = {i: Int -> i * i}
    //ラムダの型推論上のを二つの方法で
    val square1 = {i: Int -> i * i
    }

    val square2: (Int)->Int={
        i -> i*i
    }

    println(square(2))
    println(square1(3))
    println(square2(4))

    //引数が一つの時は暗黙の変数itを使える
    val square3: (Int) -> Int = {
        it * it
    }
    println(square3(5))

    //上記のfirst(2つめの引数を汎用的にしたやつを使って空白文字列判定をラムダでやる
    fun firstWhitespace(str: String): Int{
        val isWhitespace: (Char)-> Boolean = {
            it.isWhitespace()
        }
        return first(str, isWhitespace)
    }


    println(firstWhitespace(" dfd fdfd"))

    //ラムダ式を引数に直接埋め込む
    fun firstWhitespace2(str: String): Int =
        first(str,{it.isWhitespace()})
    println(firstWhitespace2("d fdfd"))

    //上の書き方の特別な書き方構文糖衣というらしい
    //引数の最後にラムダ式がある場合は外だしできる
    fun firstWhitespace3(str: String): Int =
            first(str) {it.isWhitespace()}
    println(firstWhitespace3("dd fdfdf"))

    //インライン関数
    //引数の関数オブジェクトがコンパイル時にインライン展開される関数
    //先ずは普通の関数
    fun log(debug: Boolean = true, message: () -> String){
        if(debug){
            println(message())
        }
    }
    log{"出力される"}
    log(false) {"出力されない"}

    //インライン関数
    //生成されるバイトコードが違うって
    //なんかコンパイルエラーになる
    /*inline fun log2(debug: Boolean = true, message: () -> String){
        if(debug){
            println(message())
        }
    }
    log2{"出力される"}
    log2(false) {"出力されない"}*/

    //無名関数
    //関数オブジェクトを直接描く方法
    //ラムダとの違い
    //リターンがあるかないか
    //ラムダ式
    val square1: (Int)-> Int = {i:Int -> i*i}
    //無名関数
    val square2: (Int)->Int= fun(i: Int): Int{ return i*i}
    //無名関数省略
    val square3: (Int)->Int = fun(i:Int) = i*i
}
class MyIteretor{
    operator fun hasNext(): Boolean = Math.random() < 0.5
    operator fun next(): String = "Hello"
}
class MyIterable{
    operator fun iterator() = MyIteretor()
}